#!/bin/sh

work_dir=$(dirname $0)

echo 'POSTGRES_PASSWORD='$(pass perso/Servers/Nextcloud/pgsql) >$work_dir/secrets.env
echo 'NEXTCLOUD_ADMIN_PASSWORD='$(pass perso/Servers/Nextcloud/default_admin) >>$work_dir/secrets.env
echo 'SMTP_NAME=system@lordran.net' >>$work_dir/secrets.env
echo 'SMTP_PASSWORD='$(pass perso/Email/Gandi/system@lordran.net) >>$work_dir/secrets.env
echo 'REDIS_HOST_PASSWORD='$(pass perso/Servers/Redis/password) >>$work_dir/secrets.env

S3_ACCESS_KEY=$(pass perso/Servers/Wasabi/Nextcloud/s3_access_key) \
    S3_SECRET_KEY=$(pass perso/Servers/Wasabi/Nextcloud/s3_secret_key) \
    CONFIG=\$CONFIG \
    envsubst > $work_dir/s3.config.php < $work_dir/s3.config.php.tmpl
